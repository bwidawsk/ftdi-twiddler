#include <assert.h>
#include <ftdi.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define U_TO_S(u) (u * 1000000)

static void
print_help(char *argv0)
{
	fprintf(stdout, "Usage: %s [-t] [-d secs]\n", argv0);
	fprintf(stdout, "\t-d\tDisengage relay\n");
	fprintf(stdout, "\t-e\tEngage relay (behavior depends on wire configuration)\n");
	fprintf(stdout, "\t-r\tPerform a hard reboot (-o followed by -t)\n");
	fprintf(stdout, "\t-o\tOFF PLATFORM - perform the 4second override (equivalent to -d6, not a typo)\n");
	fprintf(stdout, "\t-s\tDelay [seconds] to leave toggled on (default = 1)\n");
	fprintf(stdout, "\t-t\tToggle mode: on, then off with delay secs in between. (Default)\n");
	fprintf(stdout, "\t-4\tTarget relay 4\n");
	fprintf(stdout, "\t-3\tTarget relay 3\n");
	fprintf(stdout, "\t-2\tTarget relay 2\n");
	fprintf(stdout, "\t-1\tTarget relay 1\n");
}

int main(int argc, char *argv[])
{
	struct ftdi_context *ftdi;
	uint8_t data = 0;
	bool turn_on = true, turn_off = true, follow_toggle = false;
	int ret, opt;
	int dsecs = U_TO_S(1);
	int which = 0;

	while ((opt = getopt(argc, argv, "deros:t4321h")) != -1) {
		switch (opt) {
		case 't':
			turn_on = true;
			turn_off = true;
			break;
		case 'o':
			dsecs = U_TO_S(6);
			break;
		case 's':
			dsecs = U_TO_S(atoi(optarg));
			break;
		case 'e':
			turn_on = true;
			turn_off = false;
			break;
		case 'd':
			turn_on = false;
			turn_off = true;
			break;
		case 'r':
			dsecs = U_TO_S(6);
			turn_on = true;
			turn_off = true;
			follow_toggle = true;
			break;
		case '4':
		case '3':
		case '2':
		case '1':
			which |= 1 << (opt - '1');
			break;
		case 'h':
		default:
			print_help(argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	ftdi = ftdi_new();
	assert(ftdi != NULL);

	ret = ftdi_usb_open(ftdi, 0x0403, 0x6001);
	assert(ret >= 0);

	ret = ftdi_set_bitmode(ftdi, 0xFF, BITMODE_BITBANG);
	assert(ret >= 0);

	ret = ftdi_set_baudrate(ftdi, 9600);
	assert(ret >= 0);

	ret = ftdi_set_line_property(ftdi, BITS_8, STOP_BIT_1, NONE);
	assert(ret == 0);

again:
	if (turn_on) {
		data |= which;
		ret = ftdi_write_data(ftdi, &data, 1);
		assert(ret == 1);
	}

	if (dsecs)
		usleep(dsecs);

	if (turn_off) {
		data &= ~which;
		ret = ftdi_write_data(ftdi, &data, 1);
		assert(ret == 1);
	}

	if (follow_toggle) {
		dsecs = U_TO_S(1);
		follow_toggle = false;
		goto again;
	}

	ftdi_usb_close(ftdi);
	ftdi_free(ftdi);

	exit(EXIT_SUCCESS);
}
